# Calendar_Page package

This package provides a full calendar page which consists of a header, a calendar and some statistics.

## Pubspec.yaml

To use this package you have to add the following line to your pubspec.yaml:

questionnaire:
    git:
        url: https://gitlab.com/tabi/projects/packages/calendar_page.git

## Import the Package

Use the following import in the file where you want to use the CalendarPage widget:

import 'package:calendar_page/calendar_page.dart';

## CalendarPage

The actual widget is named CalendarPage and requires two parameters:

### 1. List\<CalendarPageDayData\> calendarData;

Here the user can define a list of CalendarPageDayData which represents the dates of the experiment including the values for the status ring.
In the CalendarPageDayData, the user has to specify the day, missing, unvalidated and validated values.

### 2. Widget Function(DateTime) onDatePressed;

Here the user should provide a function with one DateTime parameter returning a Widget. This function is called when a user presses a date in the calendar.

