import 'package:calendar_page/src/responsive_ui.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'calendar_properties.dart';
import 'text_style.dart';

class CalendarHeaderWidget extends ConsumerWidget {
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    var width = MediaQuery.of(context).size.width;
    bool monthBackPossible;
    bool monthForwardPossible;

    return Stack(children: [
      Image(
        image: AssetImage('assets/images/topbar_nl.png'),
        width: width,
        fit: BoxFit.fitWidth,
      ),
      Positioned(
        bottom: 10 * y * y * y * y,
        child: Container(
          width: width,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Consumer(builder: (context, ref, _) {
                      monthBackPossible = ref.watch(calendarNotifierProvider).monthIndex > -1;
                      return InkWell(
                        child: Icon(
                          Icons.keyboard_arrow_left,
                          size: 30 * f,
                          color: monthBackPossible ? Colors.white : Colors.white.withOpacity(0.5),
                        ),
                        onTap: monthBackPossible ? () => ref.watch(calendarNotifierProvider).goBackMonth() : null,
                      );
                    }),
                  ],
                ),
              ),
              Expanded(
                  child: Column(
                children: [
                  Consumer(builder: (context, ref, _) {
                    final monthString = ref.watch(calendarNotifierProvider).monthString;
                    return Text(
                      monthString,
                      style: textStyleAkko20,
                    );
                  }),
                ],
              )),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Consumer(builder: (context, ref, _) {
                      monthForwardPossible = ref.watch(calendarNotifierProvider).monthIndex < 1;
                      return InkWell(
                        child: Icon(
                          Icons.keyboard_arrow_right,
                          size: 30 * f,
                          color: monthForwardPossible ? Colors.white : Colors.white.withOpacity(0.5),
                        ),
                        onTap: monthForwardPossible ? () => ref.watch(calendarNotifierProvider).goForwardMonth() : null,
                      );
                    }),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    ]);
  }
}
