import 'dart:io';

import 'package:flutter/material.dart';

import 'calendar_header_widget.dart';
import 'calendar_properties.dart';
import 'calendar_stats_widget.dart';
import 'calendar_widget.dart';
import 'color_pallet.dart';
import 'responsive_ui.dart';

class CalendarPage extends StatelessWidget {
  final List<CalendarPageDayData> calendarData;
  final Function(DateTime) onDatePressed;

  CalendarPage({required this.calendarData, required this.onDatePressed});

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    initialiseResponsiveUI(context);

    return Scaffold(
      // hack to set color of statusbar in iOS !!
      appBar: Platform.isIOS
          ? PreferredSize(
              preferredSize: Size.fromHeight(0),
              child: AppBar(
                backgroundColor: ColorPallet.primaryColor,
              ))
          : null,
      body: Container(
        width: width,
        height: height,
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Expanded(flex: 100, child: CalendarHeaderWidget()),
            Expanded(flex: 4, child: SizedBox()),
            Expanded(flex: 250,child: CalendarWidget(calendarData, onDatePressed)),
            Expanded(flex: 90,child: CalendarStatsWidget()),
          ],
        ),
      ),
    );
  }
}
