import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_svg/svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'calendar_properties.dart';
import 'color_pallet.dart';
import 'responsive_ui.dart';
import 'text_style.dart';

class CalendarStatsWidget extends ConsumerWidget {
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final state = ref.watch(calendarNotifierProvider);

    return Container(
      margin: EdgeInsets.symmetric(horizontal: 12 * x),
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 7 * y),
        child: IntrinsicHeight(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Expanded(
                flex: 10,
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: new BorderRadius.circular(5),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(0.25),
                        offset: Offset(0, 1),
                        blurRadius: 10,
                      ),
                    ],
                  ),
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 16 * x),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('Voortgang:', style: textStyleAkko18Black),
                        SizedBox(height: 10 * y),
                        Row(
                          children: [
                            FaIcon(FontAwesomeIcons.solidCircle, size: 15, color: ColorPallet.lightGreen),
                            SizedBox(width: 10 * x),
                            Text('${state.daysSynced} dagen verstuurd', style: textStyleAkko14Black)
                          ],
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 5 * y),
                          child: Row(
                            children: [
                              FaIcon(FontAwesomeIcons.solidCircle, size: 15, color: ColorPallet.orange),
                              SizedBox(
                                width: 10,
                              ),
                              Text('${state.daysMissing} dagen ontbreken', style: textStyleAkko14Black)
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 5 * y, bottom: 5 * y),
                          child: Row(
                            children: [
                              FaIcon(FontAwesomeIcons.solidCircle, size: 15, color: ColorPallet.veryLightBlue),
                              SizedBox(width: 10 * x),
                              Text('${state.daysToDo} dagen resterend', style: textStyleAkko14Black)
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(width: 10 * x),
              Expanded(
                  flex: 9,
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: new BorderRadius.circular(5),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.25),
                          offset: Offset(0, 1),
                          blurRadius: 10,
                        ),
                      ],
                    ),
                    child: DaysCounter(state: state),
                  )),
            ],
          ),
        ),
      ),
    );
  }
}

class DaysCounter extends StatelessWidget {
  const DaysCounter({
    Key? key,
    required this.state,
  }) : super(key: key);

  final CalendarProperties state;

  @override
  Widget build(BuildContext context) {
    final totalDays = state.daysSynced + state.daysToDo + state.daysMissing;
    if (state.daysToDo == 0 && state.daysMissing == 0) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SvgPicture.asset(
            'assets/images/congratz.svg',
            width: 40 * x,
            height: 40 * y,
          ),
          Text(
            "Onderzoek afgerond!",
            textAlign: TextAlign.center,
            style: textStyleSoho16Black,
          ),
        ],
      );
    } else {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            '${state.daysSynced}/$totalDays dagen',
            style: textStyleSoho24Black,
          ),
          Text(
            "Verstuurd",
            style: textStyleSoho16Black,
          ),
        ],
      );
    }
  }
}
