import 'package:calendar_page/src/responsive_ui.dart';
import 'package:flutter/material.dart';

import 'color_pallet.dart';

final textStyleSoho16Black = TextStyle(
  fontFamily: 'Soho Pro',
  fontWeight: FontWeight.w500,
  fontSize: 17 * f,
  color: ColorPallet.darkTextColor,
);

final textStyleSoho24 = TextStyle(
  fontFamily: 'Soho Pro',
  fontWeight: FontWeight.w500,
  fontSize: 24 * f,
  color: Colors.white,
);

final textStyleSoho24Black = TextStyle(
  fontFamily: 'Soho Pro',
  fontWeight: FontWeight.w500,
  fontSize: 24 * f,
  color: ColorPallet.darkTextColor,
);

final textStyleSoho36 = TextStyle(
  fontFamily: 'Soho Pro',
  fontWeight: FontWeight.w500,
  fontSize: 36 * f,
  color: Colors.white,
);

final textStyleAkko24 = TextStyle(
  fontFamily: 'Akko Pro',
  fontWeight: FontWeight.w500,
  fontSize: 24 * f,
  color: Colors.white,
);

final textStyleAkko20 = TextStyle(
  fontFamily: 'Akko Pro',
  fontWeight: FontWeight.w500,
  fontSize: 20 * f,
  color: Colors.white,
);

final textStyleAkko18 = TextStyle(
  fontFamily: 'Akko Pro',
  fontWeight: FontWeight.w500,
  fontSize: 18 * f,
  color: Colors.white,
);

final textStyleAkko18Black = TextStyle(
  fontFamily: 'Akko Pro',
  fontWeight: FontWeight.w600,
  fontSize: 19 * f,
  color: ColorPallet.darkTextColor,
);

final textStyleAkko16 = TextStyle(
  fontFamily: 'Akko Pro',
  fontWeight: FontWeight.w400,
  fontSize: 16 * f,
  color: Colors.white.withOpacity(0.7),
  decoration: TextDecoration.none,
  decorationThickness: 0,
);

final textStyleAkko14Black = TextStyle(
  fontFamily: 'Akko Pro',
  fontWeight: FontWeight.w400,
  fontSize: 13 * f,
  color: ColorPallet.darkTextColor,
  decoration: TextDecoration.none,
  decorationThickness: 0,
);

final textStyleSourceSans16Black = TextStyle(
  fontFamily: 'SourceSans',
  fontWeight: FontWeight.w400,
  fontSize: 16 * f,
  color: ColorPallet.darkTextColor,
  decoration: TextDecoration.none,
  decorationThickness: 0,
);

final textStyleSourceSans16 = TextStyle(
  fontFamily: 'SourceSans',
  fontWeight: FontWeight.w400,
  fontSize: 16 * f,
  color: Colors.white,
  decoration: TextDecoration.none,
  decorationThickness: 0,
);

final textStyleSourceSans18 = TextStyle(
  fontFamily: 'SourceSans',
  fontWeight: FontWeight.w600,
  fontSize: 18 * f,
  color: ColorPallet.primaryColor,
  decoration: TextDecoration.none,
  decorationThickness: 0,
);

final textStyleSourceSans16Oopacity = TextStyle(
  fontFamily: 'SourceSans',
  fontWeight: FontWeight.w400,
  fontSize: 16 * f,
  color: Colors.white.withOpacity(0.7),
  decoration: TextDecoration.underline,
);

final textStyleSourceSans20 = TextStyle(
  fontFamily: 'SourceSans',
  fontWeight: FontWeight.w600,
  fontSize: 20 * f,
  color: ColorPallet.darkTextColor,
  decoration: TextDecoration.none,
  decorationThickness: 0,
);
